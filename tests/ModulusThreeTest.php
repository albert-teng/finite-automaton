<?php

namespace FSM\Test;

class ModulusThreeTest extends \PHPUnit\Framework\TestCase
{
    /**
     * An instance of Modulus Three class
     *
     * @var \FSM\FiniteAutomatonAbstract
     */
    public static $mod3;

    /**
     * Shared code to set up before running tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        self::$mod3 = new \FSM\ModulusThree();
    }

    /**
     * Test class constructor
     *
     * @return void
     */
    public function testConstructor(): void
    {
        $mod3 = new \FSM\ModulusThree();
        $this->assertEquals(get_class($mod3), 'FSM\ModulusThree');
    }

    /**
     * Test calculate - Failed - Empty input string
     *
     * @return void
     */
    public function testCalculateFailedEmptyString(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        self::$mod3->calculate('');
    }

    /**
     * Test calculate - Fail - Invalid input, decimal values
     *
     * @return void
     */
    public function testCalculateFailedInvalidInputDecimal(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        self::$mod3->calculate('42');
    }

    /**
     * Test calculate - Failed - Invalid input, non binary values mixed in
     *
     * @return void
     */
    public function testCalculateFailedInvalidInputMixedType(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        self::$mod3->calculate('100ab11');
    }

    /**
     * Test calculate - Fail - Invalid input, text values
     *
     * @return void
     */
    public function testCalculateFailedInvalidInputText(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        self::$mod3->calculate('abc');
    }

    /**
     * Test calculate - Success
     *
     * @return void
     */
    public function testCalculateSuccess(): void
    {
        $binaryString = '101011';

        $decimalValue = bindec($binaryString);
        $remainder = $decimalValue % 3;

        $result = self::$mod3->calculate($binaryString);

        $this->assertTrue(empty($result['error']));
        $this->assertEquals($result, $remainder);
    }

    /**
     * Test calculate - Success - All ones string
     *
     * @return void
     */
    public function testCalculateSuccessAllOnes(): void
    {
        $binaryString = '1111111111111111111';

        $decimalValue = bindec($binaryString);
        $remainder = $decimalValue % 3;

        $result = self::$mod3->calculate($binaryString);

        $this->assertTrue(empty($result['error']));
        $this->assertEquals($result, $remainder);
    }

    /**
     * Test calculate - Success - All zeroes string
     *
     * @return void
     */
    public function testCalculateSuccessAllZeroes(): void
    {
        $binaryString = '00000000000';

        $decimalValue = bindec($binaryString);
        $remainder = $decimalValue % 3;

        $result = self::$mod3->calculate($binaryString);

        $this->assertTrue(empty($result['error']));
        $this->assertEquals($result, $remainder);
    }

    /**
     * Test calculate - Success - Long binary string input
     *
     * @return void
     */
    public function testCalculateSuccessLongInput(): void
    {
        $binaryString = '10100100110110011010101';

        $decimalValue = bindec($binaryString);
        $remainder = $decimalValue % 3;

        $result = self::$mod3->calculate($binaryString);

        $this->assertTrue(empty($result['error']));
        $this->assertEquals($result, $remainder);
    }
}
