# Finite Automaton - Modulus 3

Finite State Machine that will take in a binary string as input and returns the remainder when dividing by 3.

## Installation:
- Clone the repo to your server.
- Run `composer install` in the repo folder.
- Once completed, visit the URL `http://[YOUR_WEB_SERVER]/finite-automaton`
- You will see the following:
![screenshot](./media/img/screenshot.png)

## Usage
- Enter a binary string and click "Calculate".
- Result will be displayed next to the form.

## Unit Testing
- In the repo folder, run the following command `php ./vendor/bin/phpunit tests`
- Alternatively, you can also execute this command `php ./vendor/bin/phpunit --testdox tests`
