<?php

namespace FSM;

use FSM\FiniteAutomatonAbstract;

class ModulusThree extends FiniteAutomatonAbstract
{
    public function __construct()
    {
        $states = ['0', '1', '2'];
        $inputOptions = ['0', '1'];
        $initialState = '0';

        $this->availableStates = $states;
        $this->currentState = $initialState;
        $this->finalStates = $states;
        $this->initialState = $initialState;
        $this->inputOptions = $inputOptions;
    }

    /**
     * Compute the finite automaton state after it receive an input.
     *
     * @param string $currentState Current state of the finite automaton machine.
     * @param string $input        One character input to calculate against current state.
     *
     * @return string
     */
    protected function computeState(string $currentState, string $input): string
    {
        $newState = '';

        switch ($currentState) {
            case '0':
                $newState = $input;
                break;

            case '1':
                if ($input == '0') {
                    $newState = '2';
                } else {
                    $newState = '0';
                }
                break;

            case '2':
                if ($input == '0') {
                    $newState = '1';
                } else {
                    $newState = '2';
                }
                break;
        }

        return $newState;
    }
}
