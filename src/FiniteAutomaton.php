<?php

namespace FSM;

abstract class FiniteAutomatonAbstract
{
    protected array $availableStates;
    protected array $finalStates;
    protected array $inputOptions;

    protected string $currentState;
    protected string $initialState;

    abstract protected function computeState(string $currentState, string $input): string;

    /**
     * Calculate the remainder based current finite automaton state.
     *
     * @param string $input Input string to be calculated.
     *
     * @throws InvalidArgumentException
     *
     * @return string
     */
    public function calculate(string $input = ''): string
    {
        if (empty($input)) {
            throw new \InvalidArgumentException('Missing input. Please specify a binary value.');
        }

        // Split input string into array of characters
        $inputArray = str_split($input);

        // Loop through the array and compute the resulting state based on both input and current state
        foreach ($inputArray as $inputValue) {
            // Make sure that the input is valid
            if (! in_array($inputValue, $this->inputOptions)) {
                throw new \InvalidArgumentException(
                    'Invalid input. Acceptable input values: [' . implode(', ', $this->inputOptions) . ']'
                );
            }

            $this->currentState = $this->computeState($this->currentState, $inputValue);
        }

        // Verify that the achieved final state is one of the valid final states
        // Note: This is here to safeguard against cases where the automata's
        //       available states does not match the final states.
        if (! in_array($this->currentState, $this->finalStates)) { // @codeCoverageIgnoreStart
            throw new \InvalidArgumentException('Invalid final state reached. Something must have gone wrong.');
        } // @codeCoverageIgnoreEnd

        return $this->currentState;
    }
}
