(function ($) {

    $(function () {
        $('#fsm_form').keypress(function (e) {
            if (e.which == 13) {
                e.preventDefault();
                e.stopPropagation();

                $('#submit').click();
            }
        });

        $('#submit').on('click', function (e) {
            e.preventDefault();
            e.stopPropagation();

            let button = $(this);
            let input_field = $('#binary_string');
            let error_container = $('#fsm_form .invalid-feedback');
            let result_container = $('#result');

            // Reset textfield display in case it was marked with error before.
            input_field.removeClass('is-invalid');
            error_container.empty().hide();

            // Reset result container
            result_container.addClass('d-none');
            result_container.find('.form-control-plaintext').empty();

            // Show loading spinner on button
            button.children().addClass('d-none');
            button.find('.btn_display.loading').removeClass('d-none');

            $.ajax({
                url: './ajax.php',
                type: 'POST',
                dataType: 'json',
                data: $('#fsm_form').serialize(),
                success: function (returned_data) {
                    result_container.removeClass('d-none');

                    for (const key in returned_data) {
                        if (returned_data.hasOwnProperty(key)) {
                            $(`#result .${key}_value`).text(returned_data[key]);
                        }
                    }
                },
                error: function (jqXHR) {
                    $('#fsm_form .invalid-feedback').text(jqXHR.responseJSON.error).show();
                },
                complete: function () {
                    button.children().addClass('d-none');
                    button.find('.btn_display.default').removeClass('d-none');
                }
            });
        });
    }); //-- document.ready

})(jQuery);
