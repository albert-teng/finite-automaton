<?php

require './src/FiniteAutomaton.php';
require './src/ModulusThree.php';

// Retrieve form value
$binaryString = $_POST['binary_string'];

// Convert binary string to decimal and find the modulus of 3 as control.
$decimalValue = bindec($binaryString);
$control_result = $decimalValue % 3;

try {
    // Initiate finite state machine and calculate the remainder.
    $modulus3 = new \FSM\ModulusThree();
    $fsm_result = $modulus3->calculate($binaryString);

    $return_array = [
        'control' => $control_result,
        'decimal' => $decimalValue,
        'fsm' => $fsm_result
    ];

    echo json_encode($return_array);
} catch (\InvalidArgumentException $iae) {
    http_response_code(400);
    echo json_encode(['error' => $iae->getMessage()]);
} catch (\Exception $e) {
    http_response_code(500);
    echo json_encode(['error' => 'An error occurred.']);
}
